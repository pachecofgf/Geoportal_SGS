<% // quicklinkBody.jsp - Links page (JSF body) %>
 <!-- Catalog Search Widget -->  
 	
 
 <script type="text/javascript">
/**
Submits from when on enter.
@param event The event variable
@param form The form to be submitted.
**/
$(function() { 
	
	 $('#geoservicios').puipanel();  
});  
 
</script>

<div id="geoservicios" style="margin-bottom:20px; width:800px""  title="Geoservicios disponibles">  
<br>
<B>Listado de Geoservicios WMS</B>
<br>
Para ser consumidos desde software GIS
<br>

<B>Amenaza Volc�nica</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Amenaza_Volcanica/Amenaza_Volcanica/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Amenaza_Volcanica/Amenaza_Volcanica/MapServer/WMSServer</a>
<br>
<br>

<B>Amenaza por Movimientos en Masa</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Mapa_Amenaza_MM/Mapa_Amenaza_MM/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Mapa_Amenaza_MM/Mapa_Amenaza_MM/MapServer/WMSServer</a>
<br>
<br>

<B>Atlas Geol�gico Colombiano</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Atlas_Geologico_Colombiano/Atlas_Geologico_Colombia/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Atlas_Geologico_Colombiano/Atlas_Geologico_Colombia/MapServer/WMSServer</a>
<br>
<br>

<B>Estaciones GNSS</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Estaciones_GNSS/Estaciones_GNSS_SGC_2014/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Estaciones_GNSS/Estaciones_GNSS_SGC_2014/MapServer/WMSServer</a>

<br>
<br>

<B>Estado de la cartograf�a Geol�gica</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Estado_Cartografia_Geologica/Estado_Catografia_Geologica/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Estado_Cartografia_Geologica/Estado_Catografia_Geologica/MapServer/WMSServer</a>
<br>
<br>

<B>Geof�sica </B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Geofisica/Geofisica_2014/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Geofisica/Geofisica_2014/MapServer/WMSServer</a>
<br>
<br>

<B>Geomorfodin�mica Costera</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Geomorfodinamica/Geomorfodinamica/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Geomorfodinamica/Geomorfodinamica/MapServer/WMSServer</a>
<br>
<br>

<B>Mapa Geol�gico Colombiano</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Mapa_Geologico_Colombia/Mapa_Geologico_Colombia/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Mapa_Geologico_Colombia/Mapa_Geologico_Colombia/MapServer/WMSServer</a>
<br>
<br>

<B>Mapa de Intensidades M�ximas Observadas</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Mapa_Intensidad_Maxima_Observada_Colombia/Mapa_Intensidades_Maximas_Colombia/MapServer/WMSServer" style= "font-size:12px">http://srvags.sgc.gov.co/arcgis/services/Mapa_Intensidad_Maxima_Observada_Colombia/Mapa_Intensidades_Maximas_Colombia/MapServer/WMSServer</a>
<br>
<br>


<B>Mapa Inventario Muestra Litoteca</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Mapa_Inventario_Muestra_Litoteca/Mapa_Inventario_Muestra_Litoteca/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Mapa_Inventario_Muestra_Litoteca/Mapa_Inventario_Muestra_Litoteca/MapServer/WMSServer</a>
<br>
<br>


<B>Mapa Metalog�nico Colombia</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Mapa_Metalogenico_Colombia_2002/Mapa_Metalogenico_Colombia_2002/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Mapa_Metalogenico_Colombia_2002/Mapa_Metalogenico_Colombia_2002/MapServer/WMSServer</a>
<br>
<br>

<B>Mapa Potencial Carbon�fero de Colombia</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Mapa_Metalogenico_Colombia_2002/Mapa_Metalogenico_Colombia_2002/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Mapa_Metalogenico_Colombia_2002/Mapa_Metalogenico_Colombia_2002/MapServer/WMSServer</a>
<br>
<br>

<B>Mapa Metalog�nico Colombia</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/Mapa_Potencial_Carbonifero_Colombia/Mapa_Potencial_Carbonifero_Colombia/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/Mapa_Potencial_Carbonifero_Colombia/Mapa_Potencial_Carbonifero_Colombia/MapServer/WMSServer</a>
<br>
<br>

<B>Metadato Geogr�fico</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/METG/METADATO/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/METG/METADATO/MapServer/WMSServer</a>
<br>
<br>

<B>Reporte Geol�gico de Termales</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/MTER/Reporte_Geologico_Termales/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/MTER/Reporte_Geologico_Termales/MapServer/WMSServer</a>
<br>
<br>

<B>Zonas de exploraci�n Criterio Hidrogeol�gico</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/PEXAS/PEXAS_Zonas_Exploracion_Critero_Hidrogeologico/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/PEXAS/PEXAS_Zonas_Exploracion_Critero_Hidrogeologico/MapServer/WMSServer</a>
<br>
<br>

<B>Zonas de Exploraci�n Criterio Demanda</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/PEXAS/PEXAS_Zonas_Explotacion_Criterio_Demanda/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/PEXAS/PEXAS_Zonas_Explotacion_Criterio_Demanda/MapServer/WMSServer</a>
<br>
<br>

<B>Diferenciaci�n Rocas Sedimentarias y Dep�sitos</B>
<br>
<a href="http://srvags.sgc.gov.co/arcgis/services/PEXAS/PEXAS_Diferenciacion_Rocas_Sedimentarias_y_Depositos/MapServer/WMSServer" style= "font-size:13px">http://srvags.sgc.gov.co/arcgis/services/PEXAS/PEXAS_Diferenciacion_Rocas_Sedimentarias_y_Depositos/MapServer/WMSServer</a>
<br>
<br>

</div>

 <!-- 
<script type="text/javascript"     src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=1.3"></script>      
<script type="text/javascript"    src="http://localhost:8080/geoportal/widgets/searchjs.jsp"></script>
 <iframe id="frame" src="http://www.sgc.gov.co"  width="900px"  height="1200px" scrolling="yes" >
 </iframe> 
 <script type="text/javascript">
 function resizeIframe() {
 var height = document.documentElement.clientHeight;
 height += document.getElementById('frame').offsetTop;
 height -= 5; /* whatever you set your body bottom margin/padding to be */        
 document.getElementById('frame').style.height = height +"px";
 document.getElementById('frame').style.width = 900 +"px";};
 document.getElementById('frame').onload = resizeIframe;
 window.onresize = resizeIframe;
 </script>
 -->